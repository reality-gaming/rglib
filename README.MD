RG Essentials Pack
Version 0.1

This pack contains the core 3rd party content used for RG missions and servers.

Manifest
| Mod | File | Version | Link |
|-----|------|---------|------|
| ACE Optionals | ace_compat_rhs_afrf3.pbo | 3.12.3 | https://ace3mod.com/ |
|   | ace_compat_rhs_gref3.pbo |
|   | ace_compat_rhs_usf3.pbo |
|   | ace_nouniformrestrictions.pbo |
|   | ace_particles.pbo |
|   | ace_tracers.pbo |
| ACE Adv CPR| adv_aceCPR.pbo | 1.5.4 | http://www.armaholic.com/page.php?id=32574 |
| CTab | cTab.pbo | 2.2.1 | http://www.armaholic.com/page.php?id=22992 |
| Bamse's CTab Fix  | bamse_cTab_fix.pbo | 1.1 | http://www.armaholic.com/page.php?id=31097 |
| BDF Parachutes | BDF_paraquedas_mc1.pbo | 1.10 | http://www.armaholic.com/page.php?id=33839 |
|   | CUP_TrackedVehicles_AAV.pbo |
|   | CUP_TrackedVehicles_Core.pbo |
|   | cup_watervehicles_core.pbo |
|   | cup_watervehicles_lhd.pbo |
|   | cup_watervehicles_weapons.pbo |
|   | cup_weapons_ammoboxes.pbo |
|   | cup_weapons_backpacks.pbo |
|   | cup_weapons_javelin.pbo |
|   | cup_weapons_m136.pbo |
|   | CUP_WheeledVehicles_HMMWV.pbo |
|   | CUP_WheeledVehicles_Jackal.pbo |
|   | CUP_WheeledVehicles_LAV25.pbo |
|   | CUP_WheeledVehicles_RG31.pbo |
| jw's CH-47 Chinook | deprecated_boeing.pbo | 1.17 | http://www.armaholic.com/page.php?id=30510 |
|   | dll_tow.pbo |
| FFAA Mod | ffaa_ar_bam.pbo | 6.1.0 | https://forums.bohemia.net/forums/topic/183063-ffaa-mod-spanish-army-mod-release-thread/ |
|   | ffaa_data.pbo |
|   | ffaa_estatico.pbo |
|   | ffaa_unidades.pbo |
| Firewill's A-10 | FIR_A10A.pbo | 1.50 | http://www.armaholic.com/page.php?id=31827 |
|   | FIR_A10A_Cfg.pbo |
|   | FIR_A10C.pbo |
|   | FIR_A10C_Cfg.pbo |
| Firewill's AirWeaponSystem | FIR_AirWeaponSystem_US.pbo | 2.6 | http://www.armaholic.com/page.php?id=28646 |
|   | FIR_AirWeaponSystem_US_cfg.pbo |
|   | FIR_AWS_CustomCutscene.pbo |
| AV-8B Harrier | FIR_AV8B.pbo | 0.40 | http://www.armaholic.com/page.php?id=28646 |
|   | FIR_AV8B_Cfg.pbo |
| C-17 Globemaster | globemaster_c17.pbo | 2.3 | http://www.armaholic.com/page.php?id=25090 |
|   | wop_gui.pbo |
| GRAD Trenches | grad_trenches_assets.pbo | 1.4.91 | https://forums.bohemia.net/forums/topic/212208-grad-trenches/ |
|   | grad_trenches_functions.pbo |
|   | grad_trenches_main.pbo |
| Hidden Identity Pack V2 | hi2_balaclava.pbo | 2.1 | http://www.armaholic.com/page.php?id=24936 |
|   | hi2_gasmask.pbo |
|   | hi2_m50gasmask.pbo |
|   | hi2_neckloose.pbo |
|   | hi2_necktight.pbo |
|   | hi2_pulledup.pbo |
|   | hi2_sasshemagh.pbo |
|   | hi2_shemaghs.pbo |
|   | hi2_tacticalhoodg.pbo |
| USS Nimitz | jdg_carrier.pbo | 0.101 | https://forums.bohemia.net/forums/topic/157177-preview-release-nimitz-for-arma3-0102a/ |
|   | jdg_shooter_anim.pbo |
|   | joe_beds.pbo |
|   | joe_nauticalbridge.pbo |
|   | ttt_edenmenu.pbo |
|   | ttt_nimitzfunctions.pbo |
|   | ttt_nimtech.pbo |
|   | ttt_nimtech_aa.pbo |
|   | ttt_nimtech_acecompat.pbo |
|   | ttt_testengine.pbo |
|   | ttt_tilly2.pbo |
|   | ttt_weapons.pbo |
| TeTeT's FA-18 | js_jc_fa18.pbo | 3.2.0 | http://www.armaholic.com/page.php?id=22594 |
|   | js_jc_fa18_contrib_squads.pbo |
|   | js_jc_fa18_contrib_squads2008.pbo |
|   | js_jc_fa18_contrib_squads2017.pbo |
|   | js_jc_fa18_squads.pbo |
| SOAR MH47-E by konyo | kyo_MH47E.pbo | 1.5 | http://www.armaholic.com/page.php?id=25580 |
| RKSL Attachments Pack | rksl_attachments_core.pbo | 2.00 | http://www.armaholic.com/page.php?id=25843 |
|   | rksl_lds.pbo |
|   | rksl_pm_525.pbo |
|   | rksl_pm_ii.pbo |
|   | rksl_rmr.pbo |
| ShackTac UI | stui_core.pbo | 1.2.3 | http://www.armaholic.com/page.php?id=29327 |
|   | stui_grouphud.pbo |
|  3CB BAF Vehicles Optionals | uk3cb_baf_vehicles_hercules.pbo | 7 | https://3cbmod.wordpress.com/released-mods/3cb-baf-vehicles/ |
